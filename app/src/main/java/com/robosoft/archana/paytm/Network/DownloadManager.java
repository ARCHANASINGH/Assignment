package com.robosoft.archana.paytm.Network;

import android.content.Context;

import com.android.volley.Response;

import com.robosoft.archana.paytm.Model.HomePage;
import com.robosoft.archana.paytm.Util.Constants;

/**
 * Created by archana on 4/8/16.
 */
public class DownloadManager {
   public static void downloadData(Context ctx, Response.Listener<HomePage> listener, Response.ErrorListener errorListener){
        GsonRequest<HomePage> request = new GsonRequest<>(Constants.URL,HomePage.class,listener,errorListener);
        request.setTag(Constants.URL);
        VolleyHelper.getInstance(ctx).addToRequestQueue(request);
   }
}