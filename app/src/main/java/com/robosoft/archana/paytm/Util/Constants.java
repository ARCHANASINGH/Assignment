package com.robosoft.archana.paytm.Util;

/**
 * Created by archana on 16/8/16.
 */
public class Constants {
    public final static String URL= "https://dl.dropboxusercontent.com/s/u1rnvb8oow2xs5c/homepage.json?dl=0";
    public final static int PAGE_MARGIN = 40;
    public final static String CAROUSEL  = "carousel-1";
    public final static String ROW = "row";
    public final static int ROW_LYT = 1;
    public final static int CAROUSEL_LYT = 2;
    public final static double  CAROUSEL_ASP_RATIO_SELLERSTORE  = 2.5;
    public final static int NO_OF_PAGE_IN_CACHE = 6;
}
