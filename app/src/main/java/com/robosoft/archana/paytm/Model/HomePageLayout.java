package com.robosoft.archana.paytm.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by archana on 4/8/16.
 */
public class HomePageLayout {
    @SerializedName("items")
    public List<Item> items;
    @SerializedName("layout")
    public String layout;
    @SerializedName("name")
    public String name;
}
