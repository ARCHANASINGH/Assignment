package com.robosoft.archana.paytm.Adapter;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.robosoft.archana.paytm.Model.Item;
import com.robosoft.archana.paytm.Network.NetworkStatus;
import com.robosoft.archana.paytm.Network.VolleyHelper;
import com.robosoft.archana.paytm.R;

import java.util.List;

/**
 * Created by archana on 5/8/16.
 */
public class ProductRecycleViewAdapter extends RecyclerView.Adapter<ProductRecycleViewAdapter.ProductViewHolder> {

    private List<Item> mItemList;
    private Context mContext;
    private ImageLoader mImgLoader;
    private View mOneRow;

    public ProductRecycleViewAdapter(Context mContext, List<Item> mItemList) {
        this.mItemList = mItemList;
        this.mContext = mContext;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mOneRow = LayoutInflater.from(mContext).inflate(R.layout.horizental_list_row, parent, false);
        return new ProductViewHolder(mOneRow);
    }

    @Override
    public void onBindViewHolder(final ProductViewHolder holder, int position) {
        Item item = mItemList.get(position);
        if (!TextUtils.isEmpty(item.name)) {
            holder.mTxtProductName.setText(item.name);
        }
        if (!TextUtils.isEmpty(item.discount)) {
            holder.mTxtDiscountPercent.setText(mContext.getResources().getString(R.string.hifen) + item.discount + mContext.getResources().getString(R.string.percentage));
        }
        if (!TextUtils.isEmpty(item.actualPrice)) {
            holder.mTxtActualPrice.setText(mContext.getResources().getString(R.string.rs_str) + mContext.getResources().getString(R.string.space) + item.actualPrice);
            holder.mTxtActualPrice.setPaintFlags(holder.mTxtActualPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
        if (!TextUtils.isEmpty(item.offerPrice)) {
            holder.mTxtOfferPrice.setTypeface(Typeface.DEFAULT_BOLD);
            holder.mTxtOfferPrice.setText(mContext.getResources().getString(R.string.rs_str) + mContext.getResources().getString(R.string.space) + item.offerPrice);
        }
        downloadImg(holder, item);
    }

    private void downloadImg(final ProductViewHolder holder, Item item) {
        if(NetworkStatus.isNetworkAvailable(mContext)){
            if (URLUtil.isValidUrl(item.imageUrl)) {
                mImgLoader = VolleyHelper.getInstance(mContext).getImageLoader();
                mImgLoader.get(item.imageUrl, new ImageLoader.ImageListener() {
                    @Override
                    public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                        if (response != null) {
                            holder.mProductImg.setImageBitmap(response.getBitmap());
                        }
                    }
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        holder.mProductImg.setImageResource(R.drawable.placeholder);
                    }
                });
            }
        }else {
            holder.mProductImg.setImageResource(R.drawable.placeholder);
        }
    }

    @Override
    public int getItemCount() {
        if(mItemList==null)
             return 0;
        else
            return mItemList.size();
    }

    class ProductViewHolder extends RecyclerView.ViewHolder {
        private TextView mTxtProductName, mTxtDiscountPercent, mTxtOfferPrice, mTxtActualPrice;
        private ImageView mProductImg;

        public ProductViewHolder(View itemView) {
            super(itemView);
            mTxtProductName = (TextView) itemView.findViewById(R.id.txt_product_name);
            mTxtActualPrice = (TextView) itemView.findViewById(R.id.txt_actual_price);
            mTxtDiscountPercent = (TextView) itemView.findViewById(R.id.txt_discount_percent);
            mTxtOfferPrice = (TextView) itemView.findViewById(R.id.txt_offer_price);
            mProductImg = (ImageView) itemView.findViewById(R.id.img_product_img);

        }
    }

}
