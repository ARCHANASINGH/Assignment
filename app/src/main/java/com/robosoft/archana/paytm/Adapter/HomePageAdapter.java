package com.robosoft.archana.paytm.Adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.robosoft.archana.paytm.HomePageActivity;
import com.robosoft.archana.paytm.Model.HomePageLayout;
import com.robosoft.archana.paytm.Model.Item;
import com.robosoft.archana.paytm.R;
import com.robosoft.archana.paytm.Util.Constants;
import com.robosoft.archana.paytm.Util.ViewUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by archana on 6/8/16.
 */
public class HomePageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<HomePageLayout> mHomePageLytList;
    private ViewPager mViewPager;
    private RecyclerView mProductRecyclerView;
    private List<String> mImgUrlList = new ArrayList<>();
    private TextView mTxtRowLytName, mTxtCarouselLytName;

    private int inflateLytType;
    private CustomPagerAdapter mCustomAdapter;

    private ProductRecycleViewAdapter mProductRecycleViewAdapter;


    public HomePageAdapter(List<HomePageLayout> mHolePageLytList, HomePageActivity mContext) {
        this.mHomePageLytList = mHolePageLytList;
        this.mContext = mContext;
        List<Item>  itemList = new ArrayList<>();
        getItems(itemList);
        mProductRecycleViewAdapter = new ProductRecycleViewAdapter(mContext, itemList);
    }

    @NonNull
    private void getItems(List<Item> itemList) {

        if(mHomePageLytList!=null&&mHomePageLytList.size()!=0) {
            for (int i = 0; i < mHomePageLytList.size(); i++) {
                List<Item> items = mHomePageLytList.get(i).items;
                if(items.size()!=0&&items!=null) {
                    if (mHomePageLytList.get(i).layout != null && mHomePageLytList.get(i).layout.equals(Constants.CAROUSEL)) {
                        for (int j = 0; j < items.size(); j++) {
                            mImgUrlList.add(items.get(j).imageUrl);
                        }
                    } else if (mHomePageLytList.get(i).layout != null && mHomePageLytList.get(i).layout.equals(Constants.ROW)) {
                        for (int j = 0; j < items.size(); j++) {
                            itemList.add(items.get(j));
                        }
                    }
                }
            }

        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        View oneRow;
        if (viewType == Constants.CAROUSEL_LYT) {
            oneRow = LayoutInflater.from(mContext).inflate(R.layout.pager_row, parent, false);
            viewHolder = new PagerViewHolder(oneRow);
        } else if (viewType == Constants.ROW_LYT) {
            oneRow = LayoutInflater.from(mContext).inflate(R.layout.horizental_list, parent, false);
            viewHolder = new HorizentalListViewHolder(oneRow);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case Constants.CAROUSEL_LYT:
                setHeader(mTxtCarouselLytName, position);
                break;
            case Constants.ROW_LYT:
                setHeader(mTxtRowLytName, position);
                mProductRecycleViewAdapter.notifyDataSetChanged();
                break;
        }
    }

    private void setHeader(TextView mTxtName, int position) {
        if (!TextUtils.isEmpty(mHomePageLytList.get(position).name))
            mTxtName.setText(mHomePageLytList.get(position).name);
    }

    @Override
    public int getItemCount() {
        if(mHomePageLytList==null)
             return 0;
        else return mHomePageLytList.size();
    }

    private void setViewPager() {
        if (mViewPager != null) {
            mCustomAdapter = new CustomPagerAdapter(mContext,mImgUrlList);
            mViewPager.setAdapter(mCustomAdapter);
        }
    }

    private void setProductRecyclerViewAdapter() {
        if (mProductRecyclerView != null) {
            mProductRecyclerView.setLayoutManager(com.robosoft.archana.paytm.Util.ViewUtils.setLinearLayoutManager(LinearLayoutManager.HORIZONTAL, mContext));
            mProductRecyclerView.setAdapter(mProductRecycleViewAdapter);
        }
    }

    class PagerViewHolder extends RecyclerView.ViewHolder {

        public PagerViewHolder(View itemView) {
            super(itemView);
            mTxtCarouselLytName = (TextView) itemView.findViewById(R.id.lyt_name);
            mViewPager = (ViewPager) itemView.findViewById(R.id.viewpager);
            mViewPager.setClipToPadding(false);
            mViewPager.setPageMargin(Constants.PAGE_MARGIN);
            mViewPager.setOffscreenPageLimit(Constants.NO_OF_PAGE_IN_CACHE);
            ViewGroup.LayoutParams params = mViewPager.getLayoutParams();
            int screenWidth = ViewUtils.getScreenWidth((Activity) mContext);
            params.height = (int) ((double) screenWidth / Constants.CAROUSEL_ASP_RATIO_SELLERSTORE);
            params.width  = screenWidth;
            setViewPager();
        }
    }

    class HorizentalListViewHolder extends RecyclerView.ViewHolder {
        public HorizentalListViewHolder(View itemView) {
            super(itemView);
            mTxtRowLytName = (TextView) itemView.findViewById(R.id.lyt_name);
            mProductRecyclerView = (RecyclerView) itemView.findViewById(R.id.product_recycler);
            setProductRecyclerViewAdapter();
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mHomePageLytList.get(position).layout.equals(Constants.CAROUSEL)) {
            inflateLytType = Constants.CAROUSEL_LYT;
        } else if (mHomePageLytList.get(position).layout.equals(Constants.ROW)) {
            inflateLytType = Constants.ROW_LYT;
        }
        return inflateLytType;
    }
}

