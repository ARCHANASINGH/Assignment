package com.robosoft.archana.paytm;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.ImageView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.robosoft.archana.paytm.Adapter.HomePageAdapter;
import com.robosoft.archana.paytm.Model.HomePage;
import com.robosoft.archana.paytm.Model.HomePageLayout;
import com.robosoft.archana.paytm.Network.DownloadManager;
import com.robosoft.archana.paytm.Network.NetworkStatus;
import com.robosoft.archana.paytm.Network.VolleyHelper;
import com.robosoft.archana.paytm.Util.Constants;
import com.robosoft.archana.paytm.Util.ViewUtils;

import java.util.List;

public class HomePageActivity extends AppCompatActivity{

    private ImageView mImgErrorMsg;
    private ProgressDialog mProgressDialog;
    private RecyclerView mHomePageRecycleView;
    private CoordinatorLayout mCoordinatorLayout;
    private FloatingActionButton mFloatBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        initUi();
        downloadServerData();
    }

    private void initUi() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mProgressDialog = new ProgressDialog(this);
        mCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.container);
        mHomePageRecycleView = (RecyclerView) findViewById(R.id.layout_recycler);
        mImgErrorMsg = (ImageView) findViewById(R.id.networkerrorimg);
        mFloatBtn = (FloatingActionButton) findViewById(R.id.fab);
    }

    private void downloadServerData() {
        if(NetworkStatus.isNetworkAvailable(this)) {
            if(URLUtil.isValidUrl(Constants.URL)){
            ViewUtils.showProgressDialog(mProgressDialog);
            DownloadManager.downloadData(this, new Response.Listener<HomePage>() {
                @Override
                public void onResponse(HomePage response) {
                    ViewUtils.dismissProgressDialog(mProgressDialog);
                    if (response != null) {
                        if(response.homePageLayout.size()!=0&&response.homePageLayout!=null){
                            setHomepagerAdapter(response.homePageLayout);
                        }
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ViewUtils.dismissProgressDialog(mProgressDialog);
                    mImgErrorMsg.setVisibility(View.VISIBLE);
                }
            });

        }
        }
        else {
            mFloatBtn.setVisibility(View.VISIBLE);
            setSnackBar();
        }
    }

    private void setHomepagerAdapter(List<HomePageLayout> homePageLayoutList) {
        mFloatBtn.setVisibility(View.GONE);
        HomePageAdapter homePageAdapter = new HomePageAdapter(homePageLayoutList, this);
        mHomePageRecycleView.setLayoutManager(ViewUtils.setLinearLayoutManager(LinearLayoutManager.VERTICAL, this));
        mHomePageRecycleView.setAdapter(homePageAdapter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        ViewUtils.dismissProgressDialog(mProgressDialog);
        mProgressDialog = null;
        VolleyHelper.getInstance(this).cancelRequest(Constants.URL);
    }

    private void setSnackBar() {
        Snackbar.make(mCoordinatorLayout, getResources().getString(R.string.no_internet_connection), Snackbar.LENGTH_LONG)
                .setAction(getResources().getString(R.string.retry), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (NetworkStatus.isNetworkAvailable(getApplicationContext())) {
                            downloadServerData();
                        }
                    }
                }).show();
    }

    public void onClick(View v){
        setSnackBar();
    }
    // test purpose
}
